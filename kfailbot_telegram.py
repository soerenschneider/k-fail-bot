import logging
import telegram

from telegram.ext import Updater
from telegram.ext import CommandHandler, MessageHandler, Filters, CallbackQueryHandler
from telegram import InlineKeyboardButton, InlineKeyboardMarkup

_unsubscribe_prefix = "unsubscribe_from_line_"
_cmd_subscribe = "subscribe"


class KfailbTelegramBot:
    def __init__(self, token, db):
        self._db = db
        self.updater = Updater(token=f'{token}')
        self.updater.dispatcher.add_handler(CallbackQueryHandler(self.unsubscribe_menu, pattern=f'^{_unsubscribe_prefix}'))
        self._bot = telegram.Bot(token=f'{token}')

        dispatcher = self.updater.dispatcher
        
        unsubscribe_handler = CommandHandler('unsubscribe', self.unsubscribe)
        dispatcher.add_handler(unsubscribe_handler)
        
        subscribe_handler = CommandHandler('subscribe', self.subscribe)
        dispatcher.add_handler(subscribe_handler)

        info_handler = CommandHandler('info', self.cmd_info)
        dispatcher.add_handler(info_handler)

        self.updater.start_polling()

    @staticmethod
    def start(bot, update):
        logging.info(update.message.chat_id)
        logging.warning(update.message.text)
        bot.send_message(chat_id=update.message.chat_id, text="Please send a message with the line!")

    @staticmethod
    def unsubscribe_menu_keyboard(lines):
        keyboard = [[InlineKeyboardButton(f'Line {x}', callback_data=f'{_unsubscribe_prefix}{x}') for x in lines]]
        keyboard.append([InlineKeyboardButton('All', callback_data=f'{_unsubscribe_prefix}all'), InlineKeyboardButton('Cancel', callback_data=f'{_unsubscribe_prefix}cancel')])

        return InlineKeyboardMarkup(keyboard)

    def send_message(self, recipient, text, markdown=False):
        recipient = str(recipient)
        if markdown:
            self._bot.send_message(chat_id=recipient, text=text)
        else:
            self._bot.send_message(chat_id=recipient, text=text)

    def unsubscribe_menu(self, bot, update):
        """ Called when the user clicks on a button """
        query = update.callback_query
        data = query.data[len(_unsubscribe_prefix):]
        chat_id = update.effective_user.id

        text = "Cancelled"
        if 'all' == data:
            self._db.unsubscribe_from_all_lines(chat_id)
            text = "Unsubscribed from all lines"
        elif 'cancel' != data:
            self._db.unsubscribe_from_line(chat_id, int(data))
            text = f"Unsubscribed from line {data}"

        bot.edit_message_text(chat_id=query.message.chat_id, message_id=query.message.message_id, text=text)

    def cmd_info(self, bot, update):
        chat_id = update.message.chat_id
        lines = self._db.get_subscriptions(chat_id)
        if lines:
            lines_str = ','.join(str(x) for x in lines)
            update.message.reply_text(f'You are subscribed to {lines_str}.')
        else:
            update.message.reply_text("You are not subscribed to any line, yet. \nIn order to do so, use the command /subscribe <line>")

    def unsubscribe(self, bot, update):
        chat_id = update.message.chat_id
        lines = self._db.get_subscriptions(chat_id)
        if lines:
            markup = self.unsubscribe_menu_keyboard(lines)
            update.message.reply_text("Unsubscribe", reply_markup=markup)
        else:
            update.message.reply_text("You are not subscribed to any line, yet. \nIn order to do so, use the command /subscribe <line>")

    def subscribe(self, bot, update):
        chat_id = update.message.chat_id
        text = update.message.text
        text = text[1 + len(_cmd_subscribe):]

        try:
            line = int(text)
            self._db.subscribe_to_line(chat_id, line)

        except Exception as e:
            logging.error(f'Garbage input: {e}')
            update.message.reply_text("Could not parse line information.")
            return

        msg = f"Subscribed to line {line}"
        update.message.reply_text(msg)

