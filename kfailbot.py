import logging
import telegram
import redis
import sys
import configargparse
import hashlib

import kfailbot_db_backends

from kfailbot_hash_backend import ProcessedHashesRedisBackend

from kfailbot_telegram import KfailbTelegramBot

_bot = None
_redis = None
_args = None

_key_all = "kfailb_all"
_key_line = "kfailb_line_{}"
_last_messages = {}
_db_backend = None


def get_hash_for_incident(incident):
    return hashlib.sha256(str(incident).encode('utf-8')).hexdigest()


class ProcessedHashes:
    @staticmethod
    def filter_stale_data(hash_backend):
        """ Returns a subset of the list that has not been seen before. """
        processed_hashes = []

        ret = []

        cache_data = _hashes_backend.get_data()
        for incident in cache_data['incidents']:
            hashed = get_hash_for_incident(incident)
            processed_hashes.append(hashed)

            if not hash_backend.is_hash_processed(hashed):
                logging.debug(f'{hashed} [{incident}] not found in prev_processed_hashes')

                # attach timestamp from original object
                incident['timestamp'] = cache_data['time_stamp']
                ret.append(incident)

        hash_backend.write_processed_hashes(processed_hashes)

        return ret, []


def get_ids_for_subscriber(line):
    return _db_backend.get_subscribers(int(line))


def format_incident(incident):
    ret = ""
    ret += "*" + incident['what'] + "* \n"

    if 'direction' in incident:
        ret += "_" + incident['direction'] + "_ \n"

    if 'stations' in incident:
        for station in incident['stations']:
            ret += f"{station['station']}:{station['time']}" +"\n"
    ret += "\n"

    return ret


def notify_new_problems(incidents):
    for incident in incidents:
        message = format_incident(incident)
        recipients = get_ids_for_subscriber(incident['line'])
        for recipient in recipients:
            _bot.send_message(recipient=recipient, text=f"*{incident['line']}* {message}", markdown=True)


def notify_solved_problems(diff):
    for line in diff:
        recipients = get_ids_for_subscriber(line)
        for recipient in recipients:
            _bot.send_message(recipient=recipient, text=f"Problem mit Linie {line} behoben")


def idle():
    global _redis, _hashes_backend
    
    _redis = redis.Redis(host=_args.redis_host, port=6379, db=0)
    _hashes_backend = ProcessedHashesRedisBackend(_redis)
    pubsub = _redis.pubsub()
    pubsub.psubscribe(_args.topic)
    for msg in pubsub.listen():
        if msg['type'] == 'pmessage':
            new_data, removed_data = ProcessedHashes.filter_stale_data(_hashes_backend)
            notify_new_problems(new_data)



def init():
    level = logging.INFO
    if _args.debug:
        level = logging.DEBUG
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=level)
    logging.getLogger("telegram").setLevel(logging.WARNING)

    global _db_backend
    _db_backend = kfailbot_db_backends.DbBackend(host=_args.pg_host, user=_args.pg_user, password=_args.pg_pw, db_name=_args.pg_db)

    global _bot
    _bot = KfailbTelegramBot(_args.token, _db_backend)


def parse_args():
    """ Argparse stuff happens here. """
    parser = configargparse.ArgumentParser(prog='k-fail-bot')

    parser.add_argument('--topic', action="store", env_var="KFAILBOT_TOPIC", default="kfailb_events")
    parser.add_argument('-t', '--token', action="store", env_var="KFAILBOT_TOKEN", required=True)
    parser.add_argument('--debug', action="store", env_var="KFAILBOT_DEBUG", default=False)
    parser.add_argument('-r', '--redis-host', dest="redis_host", action="store", env_var="KFAILBOT_REDIS", required=True)
    parser.add_argument('--pg-host', dest="pg_host", action="store", env_var="KFAILBOT_PG_HOST", required=True)
    parser.add_argument('--pg-user', dest="pg_user", action="store", env_var="KFAILBOT_PG_USER", required=True)
    parser.add_argument('--pg-pw', dest="pg_pw", action="store", env_var="KFAILBOT_PG_PW", required=True)
    parser.add_argument('--pg-db', dest="pg_db", action="store", env_var="KFAILBOT_PG_DB", default="kfailbot")

    global _args
    _args = parser.parse_args()


if __name__ == '__main__':
    parse_args()
    init()
    idle()

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)
