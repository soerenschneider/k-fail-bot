CREATE TABLE IF NOT EXISTS kfailbot (
    line SMALLINT PRIMARY KEY,
    subscribers INTEGER[]
);
