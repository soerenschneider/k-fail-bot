FROM python:3.7-slim

COPY requirements.txt /opt/kfailbot/requirements.txt
WORKDIR /opt/kfailbot
RUN pip install --no-cache-dir -r requirements.txt

COPY kfailbot.py kfailbot_db_backends.py kfailbot_telegram.py kfailbot_hash_backend.py /opt/kfailbot/

RUN useradd toor
USER toor

CMD ["python3", "kfailbot.py"]
