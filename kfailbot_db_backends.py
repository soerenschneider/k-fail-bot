import psycopg2
import logging
import time


class DummyBackend:
    def __init__(self):
        self._store = map()

    def unsubscribe_from_all_lines(self, subscriber):
        pass

    def unsubscribe_from_line(self, subscriber, line):
        pass

    def subscribe_to_line(self, subscriber, line):
        pass

    def get_subscriptions(self, subscriber):
        pass

    def get_subscribers(self, line):
        pass


class DbBackend:
    _reconnect_retries = 5

    def __init__(self, host, user, password, db_name='kfailbot', table_name='kfailbot'):
        self._host = host
        self._user = user
        self._password = password
        self._table_name = table_name

        retries = self._reconnect_retries
        connected = False
        logging.info(f'Trying to connect to database on {host}')
        while retries >= 0 and not connected:
            try:
                self._db = psycopg2.connect(host=host, user=user, password=password, dbname=db_name)
                connected = True
                logging.info(f'Successfully connected to database')
            except psycopg2.OperationalError as e:
                retries -= 1
                sleep = self._reconnect_retries - retries
                logging.info(f"Can't connect to db, trying again in {sleep} seconds: {e}")
                time.sleep(sleep)

        if not connected:
            logging.fatal("Couldn't connect to database, giving up. ")
            raise Exception("Can't connect to DB.")

    def unsubscribe_from_all_lines(self, subscriber):
        if not isinstance(subscriber, int):
            raise Exception("You can only submit a single subscriber")

        with self._db.cursor() as cursor:
            sql = f"UPDATE {self._table_name} SET subscribers = array_remove(subscribers, %s)"
            cursor.execute(sql, (subscriber,))
        self._db.commit()

    def unsubscribe_from_line(self, subscriber, line):
        if not isinstance(subscriber, int):
            raise Exception("You can only submit a single subscriber")

        with self._db.cursor() as cursor:
            sql = f"UPDATE {self._table_name} SET subscribers = array_remove(subscribers, %s) WHERE line = %s"
            cursor.execute(sql, (subscriber, line))
        self._db.commit()

    def subscribe_to_line(self, subscriber, line):
        if not isinstance(subscriber, int):
            raise Exception("You can only submit a single subscriber")

        sub_list = list()
        sub_list.append(subscriber)

        with self._db.cursor() as cursor:
            sql = f"INSERT INTO {self._table_name} VALUES(%s, %s) ON CONFLICT (line) DO UPDATE SET subscribers = array_cat({self._table_name}.subscribers, %s) WHERE {self._table_name}.line = %s AND %s <> ALL ({self._table_name}.subscribers);"
            cursor.execute(sql, (line, sub_list, sub_list, line, subscriber))
        self._db.commit()

    def get_subscriptions(self, subscriber):
        with self._db.cursor() as cursor:
            sql = f"SELECT line FROM {self._table_name} WHERE %s = ANY(subscribers)"
            cursor.execute(sql, (subscriber,))
            return [r[0] for r in cursor.fetchall()]

    def get_subscribers(self, line):
        with self._db.cursor() as cursor:
            sql = f"SELECT subscribers FROM {self._table_name} WHERE line = %s"
            cursor.execute(sql, (line,))
            ret = cursor.fetchone()
            if ret:
                return ret[0]
            return []

    def delete(self):
        with self._db.cursor() as cursor:
            sql = f"DELETE FROM {self._table_name}"
            cursor.execute(sql)
        self._db.commit()
